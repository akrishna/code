#include"iostream"
#include"string.h"
#include"set"
#include"vector"
#include"algorithm"
#include"map"
#define FORi(a,b) for(int i=a;i<b;i++)
#define FORj(a,b) for(int j=a;j<b;j++)
using namespace std;
void swap(int* a, int* b) {

	int temp;
	temp=*a;
	*a=*b;
	*b=temp;

}
int quick(int* a,int p, int r) {

	int pivot,count=0,j=p-1,i;
	pivot=a[r];
	//cout << pivot << "pivot";
	FORi(p,r+1) {
		if(a[i]<pivot) {
			//cout << "sssd" ;
			count++;
			j++;
			if(i!=j) {
				//cout << i << "---" << j<<endl;
				swap(&a[i],&a[j]);
			}
		}

	}
	swap(&a[r],&a[p+count]);
	return count;
}

int highest(int *a,int p,int r,int num) {
	
	int q;
	q=quick(a,p,r);
	if((q+1)==num) {
		return a[q];
	}

	if((q+1)>num) {
		
		return highest(a,p,q-1,num);

	}
	
	else {
		return highest(a,q+1,r,num-q-1);
	}

}

int main() {
	int a[5];
	FORj(0,5) {
		
		cin >> a[j];
	}

	cout << highest(a,0,4,4);

	
	
}
