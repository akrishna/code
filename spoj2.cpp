#include"iostream"
#include"math.h"
#include"algorithm"
using namespace std;
int main() {

    int t,g,n,sum=0;
    int temp;
    cin >> t;
    float d;
    while(t--) {


        cin >> n >> g;
        int mi[g];
        sum=0;
        for(int i=0;i<g;i++) {
            cin >>  temp;
            mi[i]=min(abs(temp-1),abs(temp-n));
        }

        for(int i=0;i<g;i++) {
            cin >> temp;
            sum+=mi[i]+min(abs(temp-1),abs(temp-n));
        }

        cout << sum << endl;

    }

}
