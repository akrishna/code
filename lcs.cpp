#include"iostream"
#include"string.h"
#include"set"
#include"vector"
#include"algorithm"
#include"map"
#define FORi(a,b) for(int i=a;i<b;i++)
#define FORj(a,b) for(int j=a;j<b;j++)
using namespace std;

int main() {

	char *a="abcde",*b="abhhscde";
	int n,m;
	n=strlen(a);
	m=strlen(b);
	vector< vector<int> > value(n+1,vector<int>(m+1,0));
	vector< vector<int> > dir(n+1,vector<int>(m+1,0));
	FORi(1,n+1) {
		FORj(1,m+1) {

			if(a[i-1]==b[j-1]) {
	
				value[i][j]=1+value[i-1][j-1];
				dir[i][j]=2;
			}
			else {
				if(value[i-1][j]>value[i][j-1]) {


					value[i][j]=value[i-1][j];
					dir[i][j]=1;
				}
				else {
					value[i][j]=value[i][j-1];
					dir[i][j]=0;
					
				}

			}
		}
	}
		
	FORi(0,n+1) {
		FORj(0,m+1) {

			cout << value[i][j] << "---" << dir[i][j] << endl;
		}
		cout << "-------------------" << endl;
	}
	

}
