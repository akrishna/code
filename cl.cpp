
#include"iostream"
#include"algorithm"
#include"math.h"
using namespace std;

struct list {

    int data;
    struct list* next;

};

void insert(list** front,list** rear,int num) {

    if(*front==NULL && *rear==NULL) {

            *front=new list;
            (*front)->data=num;
            *rear=new list;
            *rear=*front;
            (*rear)->next=*front;

    }
    else {

            list *temp=new list;
            temp->data=num;
            (*rear)->next=temp;
            temp->next=*front;
            *rear=temp;


    }
}

void del(list** front,list** rear) {

     if(*front==NULL && *rear==NULL) {


         return ;
     }
     else {

     *front=(*front)->next;
     free((*rear)->next);
     (*rear)->next=*front;

     }
}

void print(list* front) {

    list *hold=front;
    while(front->next!=hold) {

        cout << front->data << endl;
        front=front->next;
    }
    cout << front->data << endl ;
}

void printrec(list* front) {

    static list *f=front;
    if(front->next==f) {

        cout << front->data << endl ;
        return ;
    }

    cout << front->data << endl;
    printrec(front->next);

}
void insert_asc(list** front,list** rear,int num) {

    if(*front==NULL && *rear==NULL) {

            *front=new list;
            (*front)->data=num;
            *rear=new list;
            *rear=*front;
            (*rear)->next=*front;

    }
    else {
            list *hold=*front;
            list* temp=*front;
            list *par=new list;
            par=temp;
            while(temp->next!=hold && temp->data<num) {
                par=temp;
                temp=temp->next;
            }

            if(temp->next==hold && (temp->data<num) ){


                list* tmp=new list;
                tmp->data=num;
                tmp->next=*front;
                (*rear)->next=tmp;
                *rear=tmp;

            }
            else if(par==temp) {

                list* tmp=new list;
                tmp->data=num;
                (*rear)->next=tmp;
                tmp->next=*front;
                *front=tmp;

            }
            else {

                list* tmp=new list;
                tmp->data=num;
                par->next=tmp;
                tmp->next=temp;
            }


    }
}

int main() {

    list *front=NULL,*rear=NULL;
    insert_asc(&front,&rear,5);
    insert_asc(&front,&rear,3);
    insert_asc(&front,&rear,8);
    //printrec(front);
   // del(&front,&rear);
    insert_asc(&front,&rear,2);
    insert_asc(&front,&rear,6);
    //del(&front,&rear);
    print(front);
}
