
#include"iostream"
#include"math.h"
#include"algorithm"
using namespace std;

int main(){
    int n,t;
    cin >> t;
    while(t--) {
    cin>>n;
    long long int arr[1001]={0};
    arr[1]=1;

    for(int i=2;i<=n;i++){
        for(int j=i;j>=1;j--){
            arr[j]=arr[j]*(j+1)-arr[j]+arr[j-1];
        }
    }
    long long int s=0;
    for(int i=1;i<=n;i++){
        s=(s%1000000007+arr[i]%1000000007)%1000000007;

    }
    cout<<s <<endl;

   }
    return 0;


}
