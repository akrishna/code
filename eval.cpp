#include"iostream"
#include"string.h"
#include"set"
#include"vector"
#include"algorithm"
#include"map"
#include"stack"
#define FORi(a,b) for(int i=a;i<b;i++)
#define FORj(a,b) for(int j=a;j<b;j++)
using namespace std;

char* di="1234567890";
char* oper="+-*/";
bool digit(char a) {

	return strchr(di,a)?1:0;
}

bool opr(char a) {

	return strchr(oper,a)?1:0;
}

int prior(char op) {

	switch(op) {

		case '+' : return 1;
			   break;
		case '-' : return 1;
			   break;
		case '*' : return 2;
			   break;
		case '/' : return 2;
			   break;
	}
}

int main() {
	
	char* str="3*2/4+2";
	vector<char> vc;
	stack<char> op;
	FORi(0,strlen(str)) {
		
		if(digit(str[i])) {
			vc.push_back(str[i]);
			//cout << str[i] << "digit";
		}
		else if(opr(str[i])){
			while(!op.empty() && prior(op.top())>=prior(str[i])) {
				
				vc.push_back(op.top());
				op.pop();
			}
			op.push(str[i]);
			//cout << str[i] << "oper";
		}
		else if(str[i]!='(') {
			op.push(str[i]);
		}
		else if(str[i]!=')') {
			while(op.top()!='(') {

				vc.push_back(op.top());
				op.pop();
			}
			op.pop();
		}
		
	}
	while(!op.empty()) {
		
		vc.push_back(op.top()) ;
		op.pop();
	}
		

	FORi(0,vc.size()) {
		cout << vc[i] ;
	}

}
