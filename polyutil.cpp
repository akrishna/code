#include "polyutil.h"

Polynomial::Polynomial() 
{
    head = NULL;
}

Polynomial::Polynomial(mono *H) 
{
    head = H;
}

Polynomial::~Polynomial() 
{
    //
}

/**
 * Inserts a monomial with coeff C and exp E
 *
 * @param int exp exponent for the monomial
 * @param float coeff coefficient for the monomial
 */
void Polynomial::insert(int exp, float coeff)
{
    head = insertUtil(head, exp, coeff);
}

/**
 * Utility for insert()
 */
mono *Polynomial::insertUtil(mono *head, int exp, float coeff)
{
    if (!coeff) return head;
    if (head == NULL) return new mono(exp, coeff);
    
    if (head->exp < exp) {
        mono *newmono = new mono(exp, coeff);
        newmono->next = head;
        return newmono;
    }

    mono *iter = head, *prev;
    while (iter != NULL) {        
        if (iter->exp > exp && (iter->next == NULL || iter->next->exp < exp)) {
            mono *newmono = new mono(exp, coeff);
            newmono->next = iter->next;
            iter->next = newmono;
            return head;
        }
        if (iter->exp == exp) {
            iter->coeff += coeff;
            return head;
        }        
        prev = iter;
        iter = iter->next;
    }

    prev->next = new mono(exp, coeff);

    return head;
}

/**
 * Utility for +
 */
mono *Polynomial::addUtil(mono *poly1, mono *poly2)
{
    mono *newHead = NULL;

    if (poly1 == NULL) return poly2;
    if (poly2 == NULL) return poly1;

    mono *iter1 = poly1, *iter2 = poly2, *iter;
    while (iter1 != NULL && iter2 != NULL) {
        if (iter1->exp > iter2-> exp) {
            if (newHead == NULL) {                
                iter = iter1;
                iter1 = iter1->next;    
                iter->next = NULL;
                newHead = iter;
            } else {
                iter->next = iter1;                
                iter1 = iter1->next;
                iter = iter->next;
                iter->next = NULL;
            }            
        } else if (iter1->exp < iter2-> exp) {
            if (newHead == NULL) {                
                iter = iter2;
                iter2 = iter2->next;
                iter->next = NULL;
                newHead = iter;
            } else {
                iter->next = iter2;
                iter2 = iter2->next;
                iter = iter->next;
                iter->next = NULL;
            }            
        } else {
            iter1->coeff += iter2->coeff;
            if (newHead == NULL) {       
                iter = iter1;
                iter1 = iter1->next;
                iter->next = NULL;
                newHead = iter;                
            } else {
                iter->next = iter1;
                iter1 = iter1->next;
                iter = iter->next;
                iter->next = NULL;
            }            
            mono *temp = iter2;
            iter2 = iter2->next;            
            delete temp;
        }
    }

    if (iter1 != NULL) iter->next = iter1;
    if (iter2 != NULL) iter->next = iter2;

    return newHead;
}

/**
 * Utility for *, polynomial param
 */
mono *Polynomial::multUtil(mono *poly1, mono *poly2)
{
    if (poly1 == NULL || poly2 == NULL) return NULL;

    mono *newHead = NULL;

    mono *iter1 = poly1, *iter2, *iter;
    while (iter1 != NULL) {
        iter2 = poly2;
        while (iter2 != NULL) {
            newHead = insertUtil(newHead, iter1->exp + iter2->exp,
                            (iter1->coeff) * (iter2->coeff));
            iter2 = iter2->next;
        }
        iter1 = iter1->next;
    }

    while (iter1 != NULL) {
        mono *temp1 = iter1;
        iter1 = iter1->next;
        delete temp1;
    }

    while (iter2 != NULL) {
        mono *temp2 = iter2;
        iter2 = iter2->next;
        delete temp2;
    }

    return newHead;
}

/**
 * Utility for *, constant param
 */
mono *Polynomial::multUtilConst(mono *head, float x)
{   
    if (x == 0.0) return NULL;
    mono *iter = head;
    while (iter != NULL) {
        iter->coeff *= x;
        iter = iter->next;
    }
    return head;
}

/**
 * Creates a copy for the current polynomial
 */
mono *Polynomial::copy(mono *head)
{
    mono *newHead = NULL;
    while (head != NULL) {
        newHead = insertUtil(newHead, head->exp, head->coeff);
        head = head->next;
    }
    return newHead;
}

/**
 * Overloads the + operator in the polynomial
 */
Polynomial& Polynomial::operator+(const Polynomial &rhs) 
{
    mono *H = addUtil(copy(head), copy(rhs.head));
    Polynomial *p = new Polynomial(H);
    return *p;
}

/**
 * Overloads the * operator in the polynomial with polynomial param
 */
Polynomial& Polynomial::operator*(const Polynomial &rhs) 
{
    mono *H = multUtil(copy(head), copy(rhs.head));
    Polynomial *p = new Polynomial(H);
    return *p;
}

/**
 * Overloads the * operator in the polynomial wtih constant param
 */
Polynomial& Polynomial::operator*(const float &rhs) 
{
    mono *H = multUtilConst(copy(head), rhs);
    Polynomial *p = new Polynomial(H);
    return *p;
}

/**
 * Utility for D()
 */
mono *Polynomial::diffUtil(mono *head) 
{
    mono *iter1 = head, *iter2 = NULL;
    while (iter1 != NULL) {
        if (iter1->exp == 0) {
            if (iter2 == NULL) return NULL;
            iter2->next = NULL;
            mono *temp = iter1;
            delete temp;
            return head;
        } else {
            iter1->coeff *= iter1->exp;
            iter1->exp--;                        
        }
        iter2 = iter1;
        iter1 = iter1->next;
    }
    return head;
}

/**
 * Utility for I()
 */
mono *Polynomial::integUtil(mono *head)
{
    mono *iter1 = head;
    while (iter1 != NULL) {
        iter1->coeff /= (iter1->exp+1);
        iter1->exp++;
        iter1 = iter1->next;
    }
    return head;
}

/**
 * Differentiation function for P
 */
Polynomial Polynomial::D()
{
    mono *H = diffUtil(copy(head));
    Polynomial p(H);
    return p;
}

/**
 * Integration function for P
 */
Polynomial Polynomial::I()
{
    mono *H = integUtil(copy(head));
    Polynomial p(H);
    return p;
}

/**
 * Value of a polynomial at x
 *
 * @param float x 
 * @return value of the polynomial at x
 */
float Polynomial::valueAt(float x)
{
    float val = 0.0;
    while (head != NULL) {
        val += head->coeff * (pow(x, head->exp));
        head = head->next;
    }

    return val;
}

/**
 * Provides a pretty print for a polynomial
 */
void Polynomial::print()
{
    mono *iter = head;
    while (iter != NULL) {
        cout << setprecision(4) << fixed << iter->coeff;
        cout << "x^" << iter->exp << ((iter->next != NULL) ? " + " : ""); 
        iter = iter->next;
    }
    cout << endl;
}
