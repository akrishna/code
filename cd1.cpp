#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;
vector<int> p,sz;
int getp(int v) {
    if (v == p[v])
        return v;
    return p[v] = getp(p[v]);
}
 
void join(int x, int y) {
    x = getp(x);
    y = getp(y);
    if (x == y)
        return;
    if (sz[x] < sz[y]) {
        p[x] = y;
        sz[y] += sz[x];
    } else {
        p[y] = x;
        sz[x] += sz[y];
    }
}
 
long long c2 (long long x) {
    return x*(x-1)/2;
}
 
int main() {
    int n,m;
    cin >> n >> m;
    sz.assign(n, 1);
    p.resize(n);
    for (int i = 0; i < n; ++i)
        p[i] = i;
    for (int i = 0; i < m; ++i) {
        int a,b;
        scanf("%d%d",&a,&b);
        join(a-1,b-1);
    }
    long long res = c2(n);
    for (int i = 0; i < n; ++i) if (getp(i) == i) {
        res -= c2(sz[i]);
    }
    cout << res << endl;
    return 0;
}
