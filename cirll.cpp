#include"iostream"
#include"stdio.h"
#include"string.h"
#include"set"
#include"map"
#include"vector"
#include"algorithm"
#define FORi(a,b) for(int i=a;i<b;i++)
#define FORj(a,b) for(int j=a;j<b;j++)
using namespace std ;

struct list {

	int data;
	struct list* next;

};

void insert_b(struct list** root,int num) {

	struct list* temp=new struct list;
	temp->data=num;
	if(*root==NULL) {

		*root=temp;
		temp->next=*root;
		return;
	}
	struct list* current=*root;
	while(current->next!=*root) {

		current=current->next;
	}

	current->next=temp;
	temp->next=*root;
	*root=temp;



}

void insert_e(struct list** root,int num) {
	struct list* temp=new struct list;
	temp->data=num;
	if(*root==NULL) {

		*root=temp;
		temp->next=*root;
	}
    struct list* current=*root;
	while(current->next!=*root) {

		current=current->next;
	}

	current->next=temp;
	temp->next=*root;

}

void print(struct list* root) {

	struct list* current=root;
	cout << current->data << endl;
	while(current->next!=root) {

		current=current->next;
		cout << current->data << endl;
	}

}
int main(){

	struct list* head=NULL;
	insert_b(&head,3);
	insert_b(&head,2);
	insert_b(&head,4);
	insert_e(&head,2);
	insert_e(&head,4);
	print(head);

}
