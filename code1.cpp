#include"iostream"
#include"string.h"
#include"math.h"
#include"algorithm"
using namespace std;
int main() {

    int t,n,k,ans,arrayans;
    string str;
    cin  >> t;
    while(t--) {



        cin >> n >> k >> ans;
        int a[n];
        for(int i=0;i<n;i++) {

            cin >> a[i] ;
        }

        cin >> str;
        if(str=="XOR") {
            arrayans=0;
            for(int i=0;i<n;i++) {

                arrayans=arrayans ^ a[i];
            }
        }
        else if(str=="OR") {
            arrayans=0;
            for(int i=0;i<n;i++) {

                 arrayans=arrayans | a[i];
            }

        }

        else {
            arrayans=1;
            for(int i=0;i<n;i++) {

                arrayans=arrayans & a[i];
            }

        }

        for(int i=0;i<k;i++) {

            if(str=="XOR") {
                ans=ans ^ arrayans;

            }
            else if(str=="OR") {

                ans=ans | arrayans;
            }

            else {

                ans=ans & arrayans;
            }

        }

        cout << ans << endl ;
    }
}
