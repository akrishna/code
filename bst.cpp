#include"iostream"
#include"string.h"
#include"set"
#include"new"
#include"stack"
#include"vector"
#include"algorithm"
#include"queue"
#include"math.h"
#include"map"
#define FORi(a,b) for(int i=a;i<b;i++)
#define FORj(a,b) for(int j=a;j<b;j++)
using namespace std;
struct tree {

	float data;
	struct tree* left;
	struct tree* right;
};
struct thtree{

    float data;
    thtree* l;
    thtree* r;
    bool fake;

};
void insert(struct tree** root,float num) {

	if(*root==NULL) {
		*root=new struct tree;
		(*root)->data=num;
		(*root)->left=NULL;
		(*root)->right=NULL;
	}
	else {

		if((*root)->data>num) {

			insert(&(*root)->left,num);

		}

		else {

			insert(&(*root)->right,num);

		}

	}

}

bool search(struct tree** root,int num,struct tree** par,struct tree** self ) {

	if(*root==NULL) {

		return false;

	}
	if((*root)->data==num) {
		*self=new struct tree;
		*self=*root;
		return true;
	}
	if(*par==NULL) {

		*par=new struct tree;
	}


	*par=*root;
	if((*root)->data>num) {

		search(&(*root)->left,num,par,self);
	}
	else {
		search(&(*root)->right,num,par,self);

	}
}

void inorder(struct tree* root,int num) {

	if(root==NULL) {

		return;
	}
	inorder(root->left,num+1);
	cout << root->data <<  " ++++  " << num << endl;
	inorder(root->right,num+1);

}

void delt(struct tree** root,int num) {

	struct tree* par=NULL,*self=NULL,*temp;
	if(search(root,num,&par,&self)==1) {
		cout << "self" << self->data << endl ;
		cout << "parent" << par->data << endl;
		if(self->left!=NULL && self->right!=NULL) {
			cout << "both child";
			temp=self->right;
			while(temp->left!=NULL) {
				par=temp;
				temp=temp->left;
			}
			self->data=temp->data;
			self=temp;


		}
		if(self->left==NULL && self->right!=NULL) {
			cout << "single right child";
			if(par->left==self) {

				par->left=self->right;
			}
			else {
				par->right=self->right;
			}

		}
		if(self->left!=NULL && self->right==NULL) {
			cout << "single left child";
			if(par->left==self) {

				par->left=self->left;
			}
			else {
				par->right=self->left;
			}

		}
		if(self->left==NULL && self->right==NULL) {
			cout << par->data << "-- "<< self->data;
			cout << "no child";
			if(par->left==self) {

				par->left=NULL;
			}
			else {
				par->right=NULL;
			}
		}
	}

}
void stinorder(tree* root) {

	stack<tree*> st;
	while(1)  {

		while(root!=NULL) {

			st.push(root);
			cout << root->data << endl;
			root=root->left;
		}

		if(st.empty()) break;
		root=st.top();
		//cout << root->data << endl;
		root=root->right;
		st.pop();
		//cout << "aa";
	}



}

void level(tree* head) {

	queue<tree*> q;
	 int l=0;
	q.push(head);
	q.push(NULL);
	int level;
	tree* temp;
	while(!q.empty()) {
        if(q.size()==1) return;
		temp=q.front();
		q.pop();
		if(temp==NULL)  { l++; q.push(NULL) ; temp=q.front(); q.pop();}
		cout << temp->data << "---"<< l <<   endl;
		if(l%2==0) {
			temp->left?q.push(temp->left):(void)0;
			temp->right?q.push(temp->right):(void)0;
		}
		else {
			temp->right?q.push(temp->right):(void)0;
			temp->left?q.push(temp->left):(void)0;
		}
	}
}

int size(tree* head) {

	if(!head) return NULL;
	if(!head->left && !head->right) return 1;
	return size(head->left)+size(head->right)+1;
}


void p(vector<int> v) {

	FORi(0,v.size()) {

		cout << v[i] << endl;
	}

}
void pr(tree* head,vector<int> c) {

	if(!head) return;
	if(head->left==NULL && head->right==NULL)  {  c.push_back(head->data); p(c); cout <<"---"  << endl ; }
	c.push_back(head->data);
	head->left?pr(head->left,c):(void)0;
	head->right?pr(head->right,c):(void)0;
}
int max1(tree* head) {

	return max((int)head->data,max(head->left?max1(head->left):0,head->right?max1(head->right):0));
}

tree* create(tree* u) {


	if(!u) return NULL;
	//if(u->left==NULL && u->right==NULL) {

		tree *v=new tree;
		 v->data=u->data;
		v->left=create(u->right);
		v->right=create(u->left);

	return v;

}


void printll(tree* head) {

    while(head!=NULL) {

        cout << head->data << endl;
        head=head->right;

    }
}

tree* convert(tree* u,tree** v) {

    tree *t1,*t2,*t3;
    if(!u) return NULL;
    if(*v==NULL) {
        *v=new tree;
    }
   // cout << u->data << endl;
    (*v)->data=u->data;
    t1=convert(u->left,&(*v)->left);
  //  while()
    (*v)->left=t1;
    if(t1!=NULL) {
    t3=t1;
    while(t3->right!=NULL) {

        t3=t3->right;
    }
    t3->right=(*v);
    }
    t2=convert(u->right,&(*v)->right);
    (*v)->right=t2;
    if(t2!=NULL)
    t2->left=(*v);

    if(t1!=NULL) return t1;
    else return *v;
}

bool check(tree* u,tree* v) {


	if(u==NULL && v==NULL) return 1;
	if(u->data!=v->data) return 0;
	if(u->left->left==NULL && u->left->right==NULL)
	return check(u->left,v->right) && check(u->right,v->left);

}

int check2(tree* u,int& found) {
	static int last=0;
	int temp1,temp2;
	if(!u) return 0;
	if(!u->left && !u->right){ last=1; return u->data; }

	temp1=check2(u->left,found);
	last==1?temp1=temp1:temp1=temp1*2;

	temp1=check2(u->right,found);
	last==1?temp2=temp2:temp2=temp2*2;

    if(!u->data==temp1+temp2) { found=found & 0; return u->data;}

}

void lv(tree* head) {

    if(!head) {cout << "aa" ;return ;}
    if(head->left==NULL && head->right==NULL) {cout << "bb" << head->data;return ;}
    cout << head->data;
    if(head->right) {  return lv(head->right);}
    else {return lv(head->left);}
}

void bd(tree* head,int l) {

    if(!head) {return ; }
    static int level=-1;
        if(l>level) {

        cout << head->data << endl;
    }
    if(head->left==NULL && head->right==NULL) {

        level=l;
    }

    bd(head->left,l+1);
    bd(head->right,l+1);

}
void rinorder(tree** head) {

    static tree* p=NULL;
    if(!*head) return ;
    rinorder(&((*head)->right));
    if((*head)->left==NULL && (*head)->right==NULL) {


        (*head)->right=p;
        cout << (*head)->data << endl;
        p=*head;
    }
    rinorder(&((*head)->left));



}
int checkcolor(tree* root,int* found) {

    if(!root) return 1;
    if(root->left==NULL && root->right==NULL) return 0;
    int flag1,flag2;
    flag1=checkcolor(root->left,found)==0?1:0;
    flag2=checkcolor(root->right,found)==0?1:0;
    if(flag1!=flag2) {
       cout << "aa";
      *found=*found && false;
    }

}

int l(tree* root) {

    if(!root) return 0;
    if(root->left==NULL && root->right==NULL) {


            return 0;
    }

    return max(l(root->left),l(root->right))+1;

}
int diameter(tree* root,int* l) {
    int left=0,right=0;
    int temp1,temp2;
    if(!root) return 0;
    if(root->left==NULL && root->right==NULL) {
        *l=0;
        return 0;
    }
    temp1=diameter(root->left,&left);
    temp2=diameter(root->right,&right);
    cout << left << "---" << right << endl;
    *l=max(left,right)+1;
    return max(max(temp1,temp2),left+right+2);
}
void ins(tree* root) {
    if(!root) return;
    static int level=0;
    int temp;
    temp=level;
    level+=1;
    ins(root->left);
    level=temp;
        cout << root->data << " --" << level <<endl;
    level+=1;
    ins(root->right);
}

void ini(tree* root) {

    int max=1;
    int level=0;
    stack<tree*> st;
  //  level++;
    st.push(root);
    tree* t;
    while(!st.empty()) {
        if(root!=NULL) {

            while(root->left!=NULL) {
                    level++;
                    root=root->left;
                    st.push(root);

            }
        }
        t=st.top();
        st.pop();
        cout << t->data << "aa" << level << endl;
        if(level>max) max=level;
        root=t->right;
        if(root!=NULL) { cout <<"vv" << endl; level++; st.push(root); }
        else level--;
       // cout << root->data <<endl;




    }
    cout << max;
}
tree*  change(tree** head) {

    tree* t;
    if(!*head) return t;

    if((*head)->data<=13 && (*head)->data>=-10) {

        change(&((*head)->left));
        change(&((*head)->right));
        return *head;

    }

    else if((*head)->data>13) {

        *head=change(&((*head)->left));



    }
    else {

        cout << "right" << (*head)->data << endl;
        *head=change(&((*head)->right));
    }



}

void tp(tree* t) {


    while(t!=NULL) {

        cout << t->data << endl;
        t=t->right;

    }
}
tree* con(tree* t) {
    tree* p;
    if(!t) return NULL;
    tree* temp=new tree;
    tree* t1=new tree;
    tree* t2=new tree;
    t1=con(t->left);
    p=t1;
    t2=con(t->right);
    if(t1!=NULL) {

        while(t1->right!=NULL) {

            t1=t1->right;
        }
    temp->data=t->data;
    t1->right=temp;
    //temp->left=t1;
    temp->right=t2;
  //  if(t2!=NULL) t2->left=temp;
   //tp(p);
  // cout <<"---------------";
    return p;
    }
    temp->data=t->data;
    temp->right=t2;
   // if(t2!=NULL) t2->left=temp;
    return temp;


}
int cmpl(tree* root,int* result) {

    if(!root) return 0;
    int temp1=cmpl(root->left,result);
    int temp2=cmpl(root->right,result);
    *result= *result && temp1-temp2<=1 && temp1-temp2>=0;
    return max(temp1,temp2)+1;

}

int sie(tree* root) {
        if(!root) return 0;
       // if(root->left==NULL && root->right==NULL) return 0;
        static int count=0;
        static int max=0;
        if(count>=max) {max=count;}
        if(root->data>(root->left==NULL?-1000:root->left->data) && root->data<(root->right==NULL?10000:root->right->data)) {
                cout << "aa" << root->data << " aa" << endl;
                if(count==0)
                count=count+3;
                else if(root->left==NULL && root->right==NULL) count=count;
                else if(root->left==NULL || root->right==NULL) count=count+1;
                else count=count+2;
        }
        else {
            cout << "bbb";
            count =0;
        }
        int temp=count;
        sie(root->left);
        count=temp>count?temp:count;
        sie(root->right);
        return max;

}
int width(tree* root) {
    if(!root) return 0;
    static int min=1000;
    static int max=-1000;
    static int count=0;
    if(count<min) min=count;
    if(count>max) max=count;
    int temp=count;
    count--;
    width(root->left);
    count=temp;
    count++;
    width(root->right);
    return max-min;
}

tree* purebst(tree* root) {

    if(!root) return NULL;
    if(root->left) {

            if(root->data<root->left->data) return root->left;
    }
    if(root->right) {


        if(root->data>root->right->data) return root->right;
    }
    tree* t1,*t2;
    t1=purebst(root->left);
    if(t1!=NULL) {

        return t1;
    }
    t2=purebst(root->right);
    if(t2!=NULL)
    return t2;

    return NULL;
}
    bool ifsub(tree* root,tree* t) {
        if(t==NULL && root==NULL) return true;
        if(t==NULL || root==NULL) return false ;
        return root->data==t->data && ifsub(root->left,t->left) && ifsub(root->right,t->right);
}
bool checkroot(tree* root,tree* t) {

    if(!root) return false;
    if(!t) return true;
    if(root->data==t->data) {

        if(ifsub(root,t)) {

            return true;
        }
    }

    if(checkroot(root->left,t)) return true;
    if(checkroot(root->right,t)) return true;
    return false;

}

void dtree(tree* t) {

    if(!t) return ;
    tree* temp=t->left;
    tree* hold=new tree;
    hold->data=t->data;
    hold->right=NULL;
    hold->left=temp;
    t->left=hold;
    dtree(temp);
    dtree(t->right);
}
void th(tree* t) {
    tree* temp,*hold;
    while(t!=NULL) {

        if(t->left==NULL) {

            cout << t->data <<endl;
            t=t->right;
        }


        else {

                temp=t->left;
                hold=t->left;
                while(temp->right!=NULL) {

                    temp=temp->right;
                }

                temp->right=t;
                t->left=NULL;
                t=hold;

        }



    }
}
void adjust(tree* t,int num) {

    if(!t) return ;
    t->data=t->data+num;
    if(t->left!=NULL) return adjust(t->left,num);
    if(t->right!=NULL) return adjust(t->right,num);


}
int csumtree(tree* t) {
    if(!t) return 0;
    if(t->left==NULL && t->right==NULL)  return t->data;
    int temp1,temp2;
    temp1=csumtree(t->left);
    temp2=csumtree(t->right);
    if(t->data<temp1+temp2) {

        t->data=temp1+temp2;
        return t->data;
    }
    else {

        if(t->left!=NULL) {

           adjust(t->left,t->data-temp1-temp2);
           return t->data;
        }

        if(t->right!=NULL) {

            adjust(t->right,t->data-temp1-temp2);
            return t->data;
        }

    }

}
int a[]={3,5,7,8,0,11,2,4,3};
char b[]={'N','N','N','L','L','L','N','L','L'};
void crt(tree** head) {
    static int ct=0;
    if(ct>8) return ;
    *head=new tree;
    cout << ct;
    (*head)->data=a[ct];
    ct++;
    (*head)->left=NULL;
    (*head)->right=NULL;
    if(b[ct-1]=='L') return ;
    crt(&((*head)->left));
    crt(&((*head)->right));
}

int getlv(tree* t) {

    if(!t) return 0;
    if(t->left==NULL && t->right==NULL) {

        return 0;

    }

     return max(getlv(t->left),getlv(t->right))+1;
}
int farthest(tree* root,tree** max,tree** min) {

    if(!root) return 0;
    if(root->left==NULL && root->right==NULL) {
        *max=root;
        *min=root;
        return 0;
    }
    tree *u1,*v1;
    int temp1=farthest(root->left,&u1,&v1);
    tree* u2,*v2;
    int temp2=farthest(root->right,&u2,&v2);
    int flag=2;
    if(root->left==NULL || root->right==NULL) flag=1;
    int temp3=getlv(root->left)+getlv(root->right)+flag;

    if(temp1>temp2 && temp1>temp3) {

            *max=u1;
            *min=v1;
            return temp1;
    }
    if(temp2>temp3 && temp2>temp1) {

        *max=u2;
        *min=v2;
        return temp2;

    }

    else {

            *max=u1;
            *min=u2;
            return temp3;
    }




}

bool redtree(tree ** root,int k) {
	if(!(*root)) {

		return true;
	}
	if((*root)->left==NULL && (*root)->right==NULL) {

		int data=(*root)->data;
		return data>k?true:false;
	}
	int temp1,temp2;
	int data=(*root)->data;
	temp1=redtree(&((*root)->left),k-data);
	temp2=redtree(&((*root)->right),k-data);
	if(temp1==0) {

		(*root)->left=NULL;

	}

	if(temp2==0) {

		(*root)->right=NULL;

	}

	return temp1 || temp2;


}

int check_height(tree *t,int* f) {

    if(!t) return 0;
    int temp1=check_height(t->left,f);
    int temp2=check_height(t->right,f);

    *f=*f&&fabs(temp1-temp2)<=1;
    return temp1>temp2?temp1+1:temp2+1;

}
bool ch_h(tree* t,int* left,int *right) {

    if(!t) return true;
    if(t->left==NULL && t->right==NULL) return true;
    int l=0,r=0;
    bool flag1=ch_h(t->left,&l,&r);
    *left=max(l,r)+1;
    l=0,r=0;
    bool flag2=ch_h(t->right,&l,&r);
    *right=max(l,r)+1;
    cout << t->data << "---->" << *left << "++"  << *right  << endl;
    return flag1 && flag2 && fabs(*left-*right)<=1;
}

void pltree(tree* t,int l) {
    if(!t) return ;
    static int level=-1;
     if(l>level) {

        cout << t->data << endl;

    }
    if(t->left==NULL && t->right==NULL) {
        cout << "---" << t->data << endl;
        level=l;
    }
    pltree(t->left,l+1);
    pltree(t->right,l+1);

}
void change (struct tree* t ) {

    if(!t) return ;
    struct tree* temp;
    temp=t->left;
    t->left=t->right;
    t->right=temp;
    change(t->left);
    change(t->right);

}

int daa(struct tree* t,int& level) {

    if(!t) return 0;
    int l=0,temp1,temp2,hold1,hold2;

    temp1=daa(t->left,l);
    hold1=l;
    l=0;
    temp2=daa(t->right,l);
    hold2=l;
    level=1+max(hold1,hold2);
    return max(max(temp1,temp2),1+hold1+hold2);

}

struct list {

    float data;
    struct list* next;
    struct list* prev;

};

struct list *dlist(tree * t) {

    if(!t) return NULL;
    struct list* temp=new list;
    temp->data=t->data;
    struct list* temp1=new struct list,*temp2=new struct list;
    temp1=dlist(t->left);
    temp2=dlist(t->right);
    if(temp1!=NULL) {
        while(temp1->next!=NULL) {
            temp1=temp1->next;
        }
        temp1->next=temp;

    }
    temp->prev=temp1;
    temp->next=temp2;
    if(temp2!=NULL)temp2->prev=temp;
    while(temp->prev!=NULL) {
        temp=temp->prev;

    }

    return temp;
}
int main() {

	struct tree *root=NULL;
	struct tree* par,*self=NULL;
	//cout << q.front() ;
	insert(&root,6);
	insert(&root,-13);
	insert(&root,14);
	insert(&root,7);
	insert(&root,8);
	insert(&root,9);
	insert(&root,5);
	self=new tree;
	self->data=21;
	self->left=NULL;
	self->right=NULL;
    root->left->left=self;
	//bd(root,0);
	//inorder(root);
	//cout << "after";
	//delt(&root,5);


	//inorder(root,0);
	//level(root);
	//cout << max1(root);
	//vector<int> vi;
	//pr(root,vi);
	//tree* head;
	//head=create(root) ;
	//level(head);
	//cout << endl ;
	//cout << check(root,head);
	//level(root);
	//int f=1;
	//check2(root,f) ;
	//cout << f << endl;
	//tree* a=NULL;
   // printll(convert(root,&a));
  // lv(root);
   //rinorder(&root);
    int* found=new int;
    *found=1;
   // checkcolor(root,found);
   // cout << *found;
    int *p=new int(0);
  // cout << meter(root,p);
  //ini(root);
 // inorder(root,0);
    //change(&root);
   // inorder(root,0);
   // tp(con(root));
  // cmpl(root,found);
   // cout << *found;
    //cout << sie(root);
  //  par=purebst(root);
    //cout << par->data << endl;
    //cout << f(5);
    bool q;
    //q=checkroot(root,t) ;
    //cout << q;
    //dtree(root);
   // inorder(root,0);
  // th(root);
  //csumtree(root);
 // inorder(root,0);
 tree* a=NULL;
// crt(&a);
 //inorder(a,0);
 //tree *t1,*t2;
// cout <<  farthest(root,&t1,&t2);
// cout << t1->data << "  " << t2->data;
   //inorder(root,0);
  // redtree(&root,20);
   cout << endl;
  // inorder(root,0);
   cout << endl;
   int *c=new int,*d=new int;
   *c=0,*d=0;
   //cout << ch_h(root,c,d);
  // pltree(root,0);
 // inorder(root,0);
 // change(root);
  //inorder(root,0);
    int level=0;
// cout << daa(root,level);
struct list* l;
l=dlist(root);
while(l!=NULL) {

    cout << l->data << endl;
    l=l->next;
}

}
