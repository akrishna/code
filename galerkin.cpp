/* Ankur Krishna Gautam 09412EN005
Mathematics and Computing - Part V
IIT - BHU, Varanasi

Galerkin Approximation Method

System 10
*/

#include<iostream>
#include<cstdio>
#include "polyutil.h"
using namespace std;
int main()
{
    int e;
    float c, k1;
    Polynomial px,qx,fx,phi;
    cout << "-----------------Galerkin Method------------------" << endl;

    cout << "\n\n\nEnter the differential equation : " << endl;

    cout << "\n\nEnter p(x) first coeffecient than exponent : \n\n TO STOP ENTER 0 AND 0 " << endl;
    cin >> c >> e;

    while(e!=0 || c!=0)
    {
        px.insert(e,c);
        cin >> c >> e;
    }
	px.print();

    cout << "\n\nEnter q(x) first coeffecient than exponent : \n\n TO STOP ENTER 0 AND 0 " << endl;
    cin >> c >> e;\

    while(e!=0 || c!=0)
    {
        qx.insert(e,c);
        cin >> c >> e;
    }
        qx.print();

    cout << "\n\nEnter f(x) first coeffecient than exponent : \n\n TO STOP ENTER 0 AND 0 " << endl;
    cin >> c >> e;

    while(e!=0 || c!=0)
    {
        fx.insert(e,c);
        cin >> c >> e;
    }
        fx.print();

    phi.insert(2,-1);
    phi.insert(1,1);
    Polynomial t1,t2,t3,t4,temp1,temp2,lhs;
    t1.insert(0,-2);
    
    t2=px*phi;
    t2=t2.D();
    t2=t2*-1;

    t3=qx*phi;
    t4=fx*phi;

    temp1=t1+t2;
    temp2=temp1+t3;

    lhs=temp2*phi;

    t4=t4.I();
    lhs=lhs.I();

    k1 = t4.valueAt(1)/lhs.valueAt(1);

    cout << "\n\n\t Alpha = " << k1 << "\n" <<endl;

    return 0;
}




/*OUTPUT :

-----------------Galerkin Method------------------



Enter the differential equation :


Enter p(x) first coeffecient than exponent :

 TO STOP ENTER 0 AND 0
0 1 0 0
0.0x^1


Enter q(x) first coeffecient than exponent :

 TO STOP ENTER 0 AND 0
1 0 0 0
1.0x^0


Enter f(x) first coeffecient than exponent :

 TO STOP ENTER 0 AND 0
-1 1 0 0
-1.0x^1


         Alpha = 0.277778


Process returned 0 (0x0)   execution time : 66.862 s
Press any key to continue.

*/
