#include"iostream"
#include"string.h"
#include"set"
#include"vector"
#include"algorithm"
#include"map"
#include"math.h"
#define FORi(a,b) for(int i=a;i<b;i++)
#define FORj(a,b) for(int j=a;j<b;j++)
using namespace std;
int size=3;
void print(char* a ) {

	FORi(0,size) {
		
		cout << a[i] ;
	}
	cout << endl;

}
void par1(char* a,int start) {
	if(start==size-1) {
		print(a);
		return;
	}
	FORi(start,size) {
		//cout << start << "---" << i << endl;
		swap(a[start],a[i]);
		par1(a,start+1);
		swap(a[start],a[i]);
	}

}

void par2(char* a,int index) {

	FORi(0,1<<index) {
		cout << "index" << i << endl;	
		FORj(0,index) {
			
			if(i&(1<<j)!=0) {
				cout << a[j];
			}
		}
		cout << endl;
	}
}

int main() {

	char a[]="abc";
	
	par2(a,3);

}
