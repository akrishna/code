    #include"iostream"
    #include <set>
    using namespace std;
    bool wordBreak(string s, set<string> &dict) {
        // Note: The Solution object is instantiated only once and is reused by each test case.
        if(s.size()==0) return true;
       // cout << s << endl;
        int i=0;
        bool temp=false;
        //if(s.size()==1) cout << "++++" << endl ;

        while(i<s.size()) {
            if(dict.find(s.substr(0,i+1))!=dict.end()) {
               // cout << s.substr(i+1) << endl;
                temp= temp || wordBreak(s.substr(i+1),dict);

            }
            i++;
        }
       // if(s.size()==1) cout << "-----" << endl ;
        return temp ;

    }


    int main() {
    string s("aaaaaaaaaaaaaaaaaaaaaaaaaaaab");
    string init[] = { "a","aa","aaa","aaaa","aaaaa","aaaaaa","aaaaaaa","aaaaaaaa","aaaaaaaaa","aaaaaaaaaa"};
    set<string> second(init,init+10);
    cout << wordBreak(s,second);
    string c("a");
    cout << "aa" << c.substr(1) ;
    }
