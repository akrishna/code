/* Ankur Krishna Gautam 09412EN005
Mathematics and Computing - Part V
IIT - BHU, Varanasi

Galerkin Approximation Method

System 10
*/

#include<iostream>
#include<cstdio>

using namespace std;

typedef struct node* NPTR;

struct node{
    int exp;
    float coeff;
    NPTR next;
};

void poly_add(NPTR, NPTR, NPTR*);
void poly_mul(NPTR, NPTR, NPTR*);

void poly_derivative(NPTR*);
void poly_integrate(NPTR*);

void poly_display(NPTR);
void poly_append(NPTR*, float, int);
void poly_add_term(NPTR*, float, int);
void poly_copy(NPTR*, NPTR);
void poly_negate(NPTR);
void poly_delete(NPTR*);

float poly_eval(NPTR);

int main()
{
    int e;
    float c, k1;
    NPTR px = NULL, qx = NULL, fx = NULL;
    NPTR phi = NULL;

    cout << "-----------------Galerkin Method------------------" << endl;

    cout << "\n\n\nEnter the differential equation : " << endl;

    cout << "\n\nEnter p(x) first coeffecient than exponent : \n\n TO STOP ENTER 0 AND 0 " << endl;
    cin >> c >> e;

    while(e!=0 || c!=0)
    {
        poly_append(&px, c, e);
        cin >> c >> e;
    }
    poly_display(px);

    cout << "\n\nEnter q(x) first coeffecient than exponent : \n\n TO STOP ENTER 0 AND 0 " << endl;
    cin >> c >> e;\

    while(e!=0 || c!=0)
    {
        poly_append(&qx, c, e);
        cin >> c >> e;
    }
    poly_display(qx);

    cout << "\n\nEnter f(x) first coeffecient than exponent : \n\n TO STOP ENTER 0 AND 0 " << endl;
    cin >> c >> e;

    while(e!=0 || c!=0)
    {
        poly_append(&fx, c, e);
        cin >> c >> e;
    }
    poly_display(fx);

    //Constructing Base Function phi1...

    poly_append(&phi, -1, 2);
    poly_append(&phi, 1, 1);

    NPTR t1 = NULL, t2 = NULL, t3 = NULL, t4 = NULL;
    NPTR temp1 = NULL, temp2 = NULL;
    NPTR lhs = NULL;
     
    poly_append(&t1, -2, 0);
    
    poly_mul(px, phi, &t2);
    poly_derivative(&t2);
    poly_negate(t2);

    poly_mul(qx, phi, &t3);
    poly_mul(fx, phi, &t4);

    poly_add(t1, t2, &temp1);
    poly_add(temp1, t3, &temp2);

    poly_mul(temp2, phi, &lhs);

    poly_integrate(&t4);
    poly_integrate(&lhs);

    k1 = poly_eval(t4)/poly_eval(lhs);

    cout << "\n\n\t Alpha = " << k1 << "\n" <<endl;

    return 0;
}

void poly_add(NPTR p, NPTR q, NPTR* sum)
{
    NPTR z;
    while(p != NULL && q != NULL)
    {
        NPTR temp;
        if(*sum == NULL)
        {
            *sum = new node;
            z = *sum;
        }
        else
        {
            z->next = new node;
            z = z->next;
        }
        if(p->exp == q->exp)
        {
            z->exp = p->exp;
            z->coeff = p->coeff + q->coeff;
            p = p->next;
            q = q->next;
        }
        else if(p->exp > q->exp)
        {
            z->exp = p->exp;
            z->coeff = p->coeff;
            p = p->next;
        }
        else
        {
            z->exp = q->exp;
            z->coeff = q->coeff;
            q = q->next;
        }
    }
    while(q != NULL)
    {
        if(*sum == NULL)
        {
            *sum = new node;
            z = *sum;
        }
        else
        {
            z->next = new node;
            z = z->next;
        }
        z->exp = q->exp;
        z->coeff = q->coeff;
        q = q->next;
    }
    while(p != NULL)
    {
        if(*sum == NULL)
        {
            *sum = new node;
            z = *sum;
        }
        else
        {
            z->next = new node;
            z = z->next;
        }
        z->coeff = p->coeff;
        z->exp = p->exp;
        p = p->next;
    }
    z->next = NULL;
}

void poly_mul(NPTR p, NPTR q, NPTR* mul)
{
    NPTR chk = q;
    while(p != NULL){
        while(q != NULL){
            int exp;
            float coeff;
            exp = p->exp + q->exp;
            coeff = (p->coeff)*(q->coeff);
            poly_add_term(mul,coeff,exp);
            q = q->next;
            }
        p = p->next;
        if(p!=NULL) q = chk;
        }
}

void poly_derivative(NPTR* in)
{
    NPTR temp=*in,prev=temp;
    while(temp!=NULL)
    {
        if(temp->exp==0)
        {
            if(temp==(*in))
            {
                *in=temp->next;
                delete temp;
            }
            else
            {
                prev->next = temp->next;
                delete temp;
            }
            temp=NULL;
            break;
        }
        temp->coeff*=temp->exp;
        temp->exp--;
        prev=temp;
        temp=temp->next;
    }
}

void poly_integrate(NPTR* in)
{
    NPTR temp=*in;
    while(temp!=NULL)
    {
        temp->exp++;
        temp->coeff/=temp->exp;
        temp=temp->next;
    }
}

void poly_display(NPTR m)
{
    while(m != NULL)
    {
        printf("%.1fx^%d   ",m->coeff,m->exp);
        m = m->next;
    }
    printf("\b\b\b\n");
}

void poly_append(NPTR* m, float c, int e)
{
    NPTR temp,p;
    temp = new node;
    temp->coeff = c;
    temp->exp = e;
    temp->next = NULL;
    p = *m;
    if(p == NULL)
        *m = temp;
    else
    {
        while(p->next != NULL)
            p = p->next;
        p->next = temp;
    }
}

void poly_add_term(NPTR* mul, float c, int e)
{
    NPTR temp,p = *mul,prev=NULL;
    if(*mul == NULL)
    {
        *mul = new node;
        (*mul)->coeff = c;
        (*mul)->exp = e;
        (*mul)->next = NULL;
    }
    else
    {
        while(p != NULL)
        {
            if(p->exp == e)
            {
                p->coeff += c;
                return;
            }
            else
            {
                if(e > p->exp)
                {
                    temp = new node;
                    temp->exp = e;
                    temp->coeff = c;
                    if(p == *mul)
                    {
                        temp->next = (*mul);
                        *mul = temp;
                        return;
                    }
                    else
                    {
                        temp->next = prev->next;
                        prev->next = temp;
                        p->next = temp;
                        return;
                    }
                }
            }
            prev = p;
            p = p->next;
        }
        temp = new node;
        temp->exp = e;
        temp->coeff = c;
        prev->next = temp;
        temp->next = NULL;
    }
}

void poly_copy(NPTR* a, NPTR b)
{
    if(b != NULL)
    {
        *a = new node;
        (*a)->exp = b->exp;
        (*a)->coeff = b->coeff;
        (*a)->next = NULL;
        poly_copy(&((*a)->next),b->next);
    }
}

void poly_negate(NPTR in)
{
    while(in!=NULL)
    {
        in->coeff*=-1;
        in=in->next;
    }
}

void poly_delete(NPTR* p)
{
    NPTR q,r=*p;
    while(r!=NULL)
    {
        q=r;
        r=r->next;
        delete q;
    }
}

float poly_eval(NPTR p)
{
    float sum=0;
    while(p!=NULL)
    {
        if(p->exp>0)
        {
            sum+=p->coeff;
        }
        p=p->next;
    }
    return sum;
}


/*OUTPUT :

-----------------Galerkin Method------------------



Enter the differential equation :


Enter p(x) first coeffecient than exponent :

 TO STOP ENTER 0 AND 0
0 1 0 0
0.0x^1


Enter q(x) first coeffecient than exponent :

 TO STOP ENTER 0 AND 0
1 0 0 0
1.0x^0


Enter f(x) first coeffecient than exponent :

 TO STOP ENTER 0 AND 0
-1 1 0 0
-1.0x^1


         Alpha = 0.277778


Process returned 0 (0x0)   execution time : 66.862 s
Press any key to continue.

*/
